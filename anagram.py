import random
from random import shuffle
from urllib.request import urlopen
from tkinter import *
t=Tk()
print("Note:this game requires internet conection")
try:
    c=Frame(t,width=500,height=500)
    h=Entry(c)
    l = Entry(c)
    k = Entry(c)
    def mixup(word):
        as_list_of_letters = list(word)
        random.shuffle(as_list_of_letters)
        return ''.join(as_list_of_letters)
    word_site =" https://raw.githubusercontent.com/first20hours/google-10000-english/master/20k.txt"
    response = urlopen(word_site)
    txt = response.read()
    WORDS = txt.splitlines()
    randomword=str(random.choice(WORDS))
    e=mixup(str(randomword[2:-1]))
    l.delete(0, END)
    l.insert(0, e)
    lengthrandomword=len(randomword)
    def x():
        global WORDS
        global l
        global randomword
        global e
        randomword=str(random.choice(WORDS))
        e=mixup(str(randomword[2:-1]))
        l.delete(0, END)
        l.insert(0, e)
    def y():
        global randomword
        l.delete(0, END)
        l.insert(0, randomword[2:-1])
    def z():
        global h
        global randomword
        if str(h.get())==randomword[2:-1]:
            l.delete(0, END)
            l.insert(0, "correct")
        else:
            l.delete(0, END)
            l.insert(0, "incorrect")
    def a():
        l.delete(0, END)
        l.insert(0, e)
    new=Button(c,text="new word",command=x)
    ans=Button(c,text="answer",command=y)
    ch=Button(c,text="check",command=z)
    sh=Button(c,text="show letters",command=a)
    c.pack()
    new.pack()
    ans.pack()
    ch.pack()
    sh.pack()
    l.pack()
    h.pack()

except:
    print("Check your connection and restart the game")