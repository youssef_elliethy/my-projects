from tkinter import *
import random
from random import shuffle
from urllib.request import urlopen
print("Note:this game requires internet conection")
try:
    t = Tk()   
    c=Frame(t)
    s=Entry(c)
    word_site =" https://raw.githubusercontent.com/first20hours/google-10000-english/master/20k.txt"
    response = urlopen(word_site)
    txt = response.read()
    WORDS = txt.splitlines()
    e=str(random.choice(WORDS))
    r=Entry(c)
    u=Entry(c)
    def mixup(word):
        as_list_of_letters = list(word)
        random.shuffle(as_list_of_letters)
        return ''.join(as_list_of_letters)
    def h():
        s.delete(0, END)
        s.insert(0,mixup(str(e)))
    def z():
        if str(r.get())==str(e):
            u.delete(0, END)
            u.insert(0, "correct")
        else:
             u.delete(0, END)
             u.insert(0, "incorrect")
    ch=Button(c,text="check",command=z)
    c.pack()
    r.pack()
    u.pack()
    ch.pack()
except:
    print("Check your internet connection and restart the game")
