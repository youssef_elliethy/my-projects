import random
import re
from tkinter import *
t=Tk()
f=Frame(t)
e=Entry(f)
v=Entry(f)
r=[]
h=10
s=''
p=[]
ind=[]
o=False
b=True
mn=False
w=Entry(f,show="*")
def qt():
    global randomword
    global h
    h=10
    randomword=str(w.get())
    v.delete(0, END)
    v.insert(0,"The word has "+str(len(randomword))+" letters")
    for x in range(0,len(randomword)):
        r.append(" ")
def q():
    try:
        global randomword
        global r
        global o
        global h
        i=str(e.get())
        randomword=list(randomword)
        for x in range(0,len(randomword)):
            if randomword[x]==i:
                r[x]=i
        for x in range(0,len(r)):
            if r[x]==" ":
                b=False
        r="".join(r)
        randomword=''.join(randomword)
        try:
            randomword2=re.sub(i, "", randomword)
        except:
            randomword=re.sub(i, "", randomword)
        if i in p:
            v.delete(0, END)
            v.insert(0, r)
            h=h-1
            o=True
            if h==0:
                v.delete(0, END)
                v.insert(0, "Hangman! The word was "+randomword)
        if i in randomword and o==False:
            v.delete(0, END)
            v.insert(0, r)
        else:
            h=h-1
        if len(randomword2)==0:
            v.delete(0, END)
            v.insert(0, r)
            v.delete(0, END)
            v.insert(0, "you won!")
        if h==0:
            v.delete(0, END)
            v.insert(0, "Hangman! The word was "+randomword)
        p.append(i)
        r=list(r)
        if "".join(r)=="".join(randomword):
            v.delete(0, END)
            v.insert(0, "you win!")
    except :
            v.delete(0, END)
            v.insert(0, "Press start first!")
def rto():
    try:
        global randomword
        v.delete(0, END)
        v.insert(0,"The word has "+str(len(randomword))+" letters")
    except:
        v.delete(0, END)
        v.insert(0,"Press start first!")
u=Button(f,text="Enter",command=q)
new=Button(f,text="Start",command=qt)
number=Button(f,text="Number of letters",command=rto)
f.pack()
new.pack()
u.pack()
number.pack()
w.pack()
e.pack()
v.pack()
