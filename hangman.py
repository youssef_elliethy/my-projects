from urllib.request import urlopen
import random
import re
from tkinter import *
t=Tk()
f=Frame(t)
e=Entry(f)
v=Entry(f)
r=[]
h=10
s=''
p=[]
ind=[]
o=False
b=True
print("Note:this game requires internet conection")
try:
    word_site =" https://raw.githubusercontent.com/first20hours/google-10000-english/master/20k.txt"
    response = urlopen(word_site) 
    txt = response.read()
    WORDS = txt.splitlines()
    randomword=str(random.choice(WORDS))
    randomword=randomword[2:-1]
    v.delete(0, END)
    v.insert(0,"The word has "+str(len(randomword))+" letters")
    for x in range(0,len(randomword)):
        r.append(" ")
    def q():
        global randomword
        global r
        global o
        global h
        i=str(e.get())
        randomword=list(randomword)
        for x in range(0,len(randomword)):
            if randomword[x]==i:
                r[x]=i
        for x in range(0,len(r)):
            if r[x]==" ":
                b=False
        r="".join(r)
        randomword=''.join(randomword)
        try:
            randomword2=re.sub(i, "", randomword)
        except:
            randomword=re.sub(i, "", randomword)
        if i in p:
            v.delete(0, END)
            v.insert(0, r)
            h=h-1
            o=True
            if h==0:
                v.delete(0, END)
                v.insert(0, "Hangman! The word was "+randomword)
        if i in randomword and o==False:
            v.delete(0, END)
            v.insert(0, r)
        else:
            h=h-1
        if len(randomword2)==0:
            v.delete(0, END)
            v.insert(0, r)
            v.delete(0, END)
            v.insert(0, "you won!")
        if h==0:
            v.delete(0, END)
            v.insert(0, "Hangman! The word was "+randomword)
        p.append(i)
        r=list(r)
        if "".join(r)=="".join(randomword):
            v.delete(0, END)
            v.insert(0, "you win!")
    def yt():
        global randomword
        global WORDS
        global h
        h=10
        randomword=str(random.choice(WORDS))
        randomword=randomword[2:-1]
        v.delete(0, END)
        v.insert(0,"The word has "+str(len(randomword))+" letters")
    def rto():
        try:
            global randomword
            v.delete(0, END)
            v.insert(0,"The word has "+str(len(randomword))+" letters")
        except:
            v.delete(0, END)
            v.insert(0,"Press start first!")
    b=Button(f,text="Enter",command=q)
    new=Button(f,text="New Word",command=yt)
    number=Button(f,text="Number of letters",command=rto)
    f.pack()   
    b.pack()
    new.pack()
    number.pack()
    e.pack()
    v.pack()
except:
    print("Check your connection and restart the game")